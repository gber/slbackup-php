<?php
/*
    slbackup-php, an administration tool for slbackup
    Copyright (C) 2007 Finn-Arne Johansen <faj@bzz.no> BzzWare AS, Norway

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

function loadConfig () {
    global $backuphost, $backupuser, $backupconf, $backupcron,
           $ssh_options, $logfile, $nonhttps, $smarty_templ, $smarty_compile ; 

    @include_once ("/etc/slbackup-php/config.php") ;

    if (empty ($nonhttps))       $nonhttps       = $_COOKIE ['nonhttps'] ;
    if (empty ($smarty_templ))   $smarty_templ   = $_COOKIE ['smarty_templ'] ;
    if (empty ($smarty_compile)) $smarty_compile = $_COOKIE ['smarty_compile'] ;

    if (empty ($backuphost)) $backuphost="localhost" ;
    if (empty ($backupuser)) $backupuser="root" ; 
    if (empty ($backupconf)) $backupconf="/etc/slbackup/slbackup.conf" ; 
    if (empty ($backupcron)) $backupcron="/etc/cron.d/slbackup" ; 
    if (empty ($ssh_options)) $ssh_options="-o StrictHostKeyChecking=no" ; 
    if (empty ($logfile)) $logfile="/var/log/slbackup/slbackup.log" ; 

    switch ($nonhttps) {
        case "warn":
        case "allow":
        case "deny":
            break ;
        default:
	    $nonhttps = "deny" ; 
	    setcookie ('nonhttps', $nonhttps) ; 
	    break ;
    }

    if (empty ($smarty_templ)) {
	$smarty_templ = realpath ("../templates") ;
        setcookie ('smarty_templ', $smarty_templ) ; 
    }

    if (empty ($smarty_compile)) {
	$smarty_compile = "/var/spool/slbackup-php" ;
	if (! is_dir ($smarty_compile))
	    $smarty_compile = "/var/tmp" ; 
        setcookie ('smarty_compile', $smarty_compile) ; 
    }
}

function loadLocale () {
    $found_locale = "";
    if (array_key_exists('locale', $_COOKIE))
	$found_locale = $_COOKIE['locale'];

    if ($found_locale == "" && $handle = @opendir ('../locales')) {
	$translations = array () ; 
	while (false !== ($file = readdir ($handle))) {
	    if (is_dir ('../locales/' . $file . '/LC_MESSAGES')) {
		$translations = array_merge ($translations, array ($file)) ; 
	    }
	}
	closedir ($handle) ; 
	$lang_accept = array () ; 
	$lang_accept = explode (",", $_SERVER['HTTP_ACCEPT_LANGUAGE']); 
	for ($i = 0 ; $i < count ($lang_accept) ; $i++ ) {
	    $lang_accept[$i] = split(";", $lang_accept[$i]) ; 
	    $lang_accept[$i] = $lang_accept[$i][0] ; 
	}

	if (!empty($lang_accept[0])) {
	    foreach ($lang_accept as $lang) {
		if (!isset ($locale_exact)) {
		    foreach ($translations as $translation) {
			if ($translation == $lang)
			    $locale_exact = $lang ; 
			elseif (!isset ($locale_match)) {
			    if (strpos ($translation, $lang) === 0)
				$locale_match = $translation ; 
			}
		    }
		}
	    }
	    if (isset ($locale_exact)) 
		$found_locale=$locale_exact ; 
	    elseif (isset ($locale_match))
		$found_locale=$locale_match ; 
	}

	$locales = array();
	$locales_utf8 = array();
	if (isset($found_locale) && !empty($lang_accept[0])) {
	    $found_locale_tuple = split('_', $found_locale);
	    if (count($found_locale_tuple) > 1) {
		$locales[] = $found_locale;
		$locales_utf8[] = $found_locale.'.UTF-8';
	    }
	    foreach ($lang_accept as $lang) {
		$lang_tuple = split('-',$lang);
		if (count($lang_tuple) > 1) {
		    $language = strtolower($lang_tuple[0]);
		    $country = strtoupper($lang_tuple[1]);
		    $locales[] = $language . '_' . $country;
		    $locales_utf8[] = $language . '_' . $country . '.UTF-8';
		}
	    }
	    $locales_all = array_merge ($locales_utf8, $locales);
	    $found_locale = setlocale (LC_ALL, $locales_all) ; 
	}

	if (isset ($found_locale))
	    setcookie ( 'locale', $found_locale) ;
	unset ($all_locales, $lang_accept, $locale_match, $locale_exact, $lang, $locales) ; 
    }

    if (isset ($found_locale)) {
	$locale = setlocale (LC_ALL, $found_locale) ; 
	if (empty($locale))
	    $locale = setlocale (LC_ALL, $found_locale . ".UTF8");
	bindtextdomain ("slbackup-php", "../locales/"); 
	bind_textdomain_codeset ("slbackup-php", "UTF-8");
	textdomain ("slbackup-php"); 
    }
    header('Content-Type: text/html; charset=UTF-8') ;
}

function xorstring ($key, $xor) {
    $keylen=strlen ($key) ; 
    if (! $keylen) return "" ; 
    $newstr="" ; 
    $index=0 ; 
    $keyindex = $index ;
    while ($index < strlen ($xor)) {
        $newstr .= chr(ord($xor[$index]) ^ ord ($key[$keyindex])) ; 
        $index ++ ;
        $keyindex= $index % $keylen ;
    }
    if ($keyindex == 0) {
        $last=strpos ($newstr, chr(0)); 
        if ($last === false) { 
            return $newstr ;
        }
        return(substr ($newstr, 0, $last)) ; 
    }
    while ($keyindex != 0) {
        $newstr .= $key[$keyindex] ; 
        $index ++ ;
        $keyindex= $index % $keylen ;
    }
    return ($newstr) ;
}

function debug ($mixed) {
    echo "<pre>\n" ; 
    print_r ($mixed);
    echo "\n</pre>\n" ; 
}

?>
