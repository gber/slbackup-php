# Vietnamese translation for slbackup-php.
# Copyright © 2007 Free Software Foundation, Inc.
# Clytie Siddall <clytie@riverland.net.au>, 2007
#
msgid ""
msgstr ""
"Project-Id-Version: slbackup-php\n"
"Report-Msgid-Bugs-To: patrick.winnertz@skolelinux.org\n"
"POT-Creation-Date: 2007-06-24 01:04+0200\n"
"PO-Revision-Date: 2007-06-25 21:32+0930\n"
"Last-Translator: Clytie Siddall <clytie@riverland.net.au>\n"
"Language-Team: Vietnamese <vi-VN@googlegroups.com>\n"
"Language: vi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: LocFactoryEditor 1.6.4a3\n"

#. Type: boolean
#. Description
#: ../templates:2001
msgid "Activate SSL support in slbackup-php?"
msgstr "Bật khả năng hỗ trợ giao thức SSL trong slbackup-php không?"

#. Type: boolean
#. Description
#: ../templates:2001
msgid ""
"Please note that SSL is needed to connect to the slbackup-php server. "
"Activating it is therefore strongly recommended."
msgstr ""
"Ghi chú rằng giao thức SSL cần thiết để kết nối tới máy phục vụ slbackup-"
"php. Vì thế rất khuyên bạn hiệu lực nó."

#. Type: boolean
#. Description
#: ../templates:2001
msgid ""
"However, SSL should also be activated in the web server which will not be "
"done by choosing this option."
msgstr ""
"Tuy nhiên, cũng cần phải hiệu lực SSL trong trình phục vụ Mạng, một công "
"việc riêng."
