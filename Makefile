PACKAGE = slbackup-php
LIBS= \
	index.php \
	sl-style.css \
	functions.php

SCRIPTS= \
	mypass.sh

CONFS= \
	apache.conf 

SAMPLES= 

DOCS= \
	COPYRIGHT

BIN= 

INSTALL     = install -p -m 755
INSTALL_DATA= install -D -p -m 644
LANG=C

prefix     = /usr/local
sysconfdir = /etc/$(PACKAGE)
sharedir   = $(prefix)/share/$(PACKAGE)
webdir     = $(sharedir)/web
scriptdir  = $(sharedir)/script
templatedir= $(sharedir)/templates
localedir  = $(sharedir)/locales
docdir     = $(prefix)/share/doc/$(PACKAGE)
htmldocdir = $(prefix)/share/doc/$(PACKAGE)/html
sampledir  = $(prefix)/share/doc/$(PACKAGE)/examples
bindir	   = $(prefix)/bin

all:

po/*.po: po/slbackup-php.pot
	cp $@ $@.bak ; \
	msgmerge $@.bak $^ > $@ 

locales/mo-stamp:
	for po in po/*.po ; do \
	    locale=$$(basename $$po .po) ; \
	    echo $$locale ; \
	    mkdir -p locales/$$locale/LC_MESSAGES ; \
	    msgfmt --statistics -o locales/$$locale/LC_MESSAGES/$(PACKAGE).mo $$po ; \
	done
	touch locales/mo-stamp

mo-files: locales/mo-stamp

po/slbackup-php.pot: src/*.php templates/*.tpl
	tsmarty2c.php templates | \
	    xgettext --add-comments --language=PHP --default-domain=slbackup-php \
	        - src/*.php --output po/slbackup-php.pot

po-files: po/*.po

install: all
	install -d $(DESTDIR)$(sharedir)
	install -d $(DESTDIR)$(sysconfdir)
	install -d $(DESTDIR)$(docdir)
	#install -d $(DESTDIR)$(htmldocdir)
	#install -d $(DESTDIR)$(sampledir)
	install -d $(DESTDIR)$(webdir)
	install -d $(DESTDIR)$(scriptdir)
	install -d $(DESTDIR)$(templatedir)
	install -d $(DESTDIR)$(localedir)
	#install -d $(DESTDIR)$(bindir)


	for locale in $$(find locales -type f -name $(PACKAGE).mo -printf "%P ") ; do \
	    install -d $(DESTDIR)$(localedir)/$$(dirname $$locale) ; \
	    $(INSTALL_DATA) locales/$$locale $(DESTDIR)$(localedir)/$$(dirname $$locale) ; \
	done

	for templates in $$(find templates -type f -name "*.tpl" -printf "%P ") ; do \
	    $(INSTALL_DATA) templates/$$templates $(DESTDIR)$(templatedir) ; \
	done

	for script in $(SCRIPTS); do \
		$(INSTALL) script/$$script $(DESTDIR)$(scriptdir); \
	done

	for lib in $(LIBS); do \
		$(INSTALL_DATA) src/$$lib $(DESTDIR)$(webdir) ; \
	done

	for conf in $(CONFS); do \
		$(INSTALL_DATA) etc/$$conf $(DESTDIR)$(sysconfdir) ; \
	done

	for doc in $(DOCS); do \
		$(INSTALL_DATA) $$doc $(DESTDIR)$(docdir) ; \
	done

	for htmldoc in $$(find doc -type f -name "*.html" -printf "%P ") ; do \
		$(INSTALL_DATA) doc/$$htmldoc $(DESTDIR)$(htmldocdir) ; \
	done

	for sample in $(SAMPLES); do \
		$(INSTALL_DATA) examples/$$sample $(DESTDIR)$(sampledir) ; \
	done

	for bin in $(BIN); do \
		$(INSTALL) bin/$$bin $(DESTDIR)$(bindir) ; \
	done


clean:
	# Remove CVS cruft
	find . -type f -name '.#*' -o -name "*.bak" -o -name "*~" | xargs -r rm -v
	# Removed completed translations
	rm -rf  locales

dist:
	debuild -us -uc

