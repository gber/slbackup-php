{*
    slbackup-php, an administration tool for slbackup
    Copyright (C) 2007  Finn-Arne Johansen <faj@bzz.no>, Bzzware AS, Norway

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*}
    {include file="header.tpl"}

    <FORM action="index.php" method=post>
	<H2>{t}Server scheduler{/t}</H2>
	{t}Backup enabled{/t} 
	<INPUT type=checkbox name=active {$active}>
	{t}Run backup at{/t}
	{html_options name=hours values=$a_hours output=$a_hours selected=$hours}
	:
	{html_options name=minutes values=$a_minutes output=$a_minutes selected=$minutes}
	<INPUT type=submit name=scheduler value="{t}Update{/t}">
	<INPUT type=reset name=reset value="{t}Reset{/t}">
	<H2>{t}Server config{/t}</H2>
	<TABLE>
	    <TR>
		<TD>{t}Address{/t}</TD>
		<TD>
		    <INPUT type=text name=server_address 
			   value="{$server_address}">
		</TD>
		<TD>{t}Destdir{/t}</TD>
		<TD>
		    <INPUT type=text name=server_destdir 
			   value="{$server_destdir}">
		</TD>
	    </TR>
	    <TR>
		<TD>{t}Servertype{/t}</TD>
		<TD>
		    {html_options name=server_type values=$types output=$types selected=$server_type}
		</TD>
		<TD>{t}User{/t}</TD>
		<TD>
		    <INPUT type=text name=server_user 
			   value="{$server_user}">
		</TD>
	    </TR>
	    <TR>
		<TD colspan=4 align=right>
		    <INPUT type=submit name=server 
			   value="{t}Update{/t}">
		    <INPUT type=reset name=reset 
			   value="{t}Reset{/t}">
		</TD>
	    </TR>

	</TABLE>
	<H2>{t}Client config{/t}</H2>
	{t}Client:{/t} 
	{html_options name=client values=$clients output=$clients selected=$client}
	<INPUT type=submit name=chooseclient value="{t}Choose{/t}">
	<INPUT type=submit name=addclient value="{t}New{/t}">
	<INPUT type=submit name=delclient value="{t}Remove{/t}">
	<TABLE>
	    <TR>
		<TD>{t}Address{/t}</TD>
		<TD>
		    <INPUT type=text name=client_address 
			   value="{$clientaddress}">
		</TD>
		<TD>{t}Backup Age{/t}</TD>
		<TD>
		    <INPUT type=text name=client_keep
			   value="{$clientkeep}">
		</TD>
	    </TR>
	    <TR>
		<TD>{t}Clienttype{/t}</TD>
		<TD>
		    {html_options name=client_type values=$types output=$types selected=$client_type}
		</TD>
		<TD>{t}User{/t}</TD>
		<TD>
		    <INPUT type=text name=client_user 
			   value="{$clientuser}">
		</TD>
	    </TR>
	    <TR>
		<TD colspan=4 align=right>
		    <INPUT type=submit name=clientconfig 
			   value="{$clientsubmit}">
		    <INPUT type=reset name=reset 
			   value="{t}Reset{/t}">
		</TD>
	    </TR>
	    <TR>
		<TD>{t}Locations:{/t}</TD>
		<TD><INPUT type=text name=newloc></TD>
		<TD><INPUT Type=submit name=addloc value="{t}Add{/t}"></TD>
	    </TR>
	    <TR>
		<TD/>
		<TD>
		    <SELECT name=location size=5>
		        {foreach item=location from=$locations}
		            <option value={$location}>
		                {$location}
		            </option>
		        {/foreach}
		</TD>
		<TD valign=top><INPUT type=submit name=delloc value="{t}Delete{/t}"></TD>
	    </TR>
	</TABLE>
    </FORM>
</HTML>
