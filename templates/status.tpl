{*
    slbackup-php, an administration tool for slbackup
    Copyright (C) 2007  Finn-Arne Johansen <faj@bzz.no>, Bzzware AS, Norway

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*}
    {include file="header.tpl"}

    <H2>{t}Clients{/t}</H2>
    <UL>
    {foreach from=$log.clients key=client item=values}
       <LI>{t 1=$client}Status for %1{/t}
       <BR>
       {if $values.started > $values.ok and $values.started > $values.failed}
           Backup started as {$values.started|date_format:"%c"}, and is still in progress<BR>
       {/if}
       {if $values.ok > $values.failed}
           {t 1=$values.ok|date_format:"%c"}OK at %1{/t}
       {elseif $values.failed <> ''}
           {t 1=$values.failed|date_format:"%c"}Failed at %1{/t}
       {elseif $values.started == ''}
           {t}Client never backed up{/t}
       {/if}
       <BR>
       {if $checkkeysandversions.clients.$client.ssh == 'ok'}
           {t}ssh-keys are working{/t}<BR>
	   {if $checkkeysandversions.clients.$client.rdiff-backup <> $checkkeysandversions.version}
	        {t}rdiff-backup-versions differ{/t}
	    {/if}
       {else}
           {t}ssh-keys are not working{/t}
       {/if}
       </LI>
    {/foreach}
    </UL>
    <H2>{t}Backup status{/t}</H2>

    {t 1=$log.end|date_format:"%c"}Backup last finished at %1{/t}<BR>
    {if $log.start > $log.end}
	{t 1=$log.start|date_format:"%c"}A new backup started at %1{/t}<BR>
    {/if}
    </Body>
</HTML>
