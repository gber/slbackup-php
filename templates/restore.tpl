{*
    slbackup-php, an administration tool for slbackup
    Copyright (C) 2007  Finn-Arne Johansen <faj@bzz.no>, Bzzware AS, Norway

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*}
    {include file="header.tpl"}

    <FORM action="index.php" method=post>
	<H2>{t}Client to restore{/t}</H2>
	{html_options name=client values=$clients output=$clients selected=$client}
	<INPUT type=submit name=restoreclient value="{t}Choose{/t}">
        {t}Locations:{/t}</TD>
	{html_options name=location values=$locations output=$locations selected=$location}
	<INPUT type=submit name=restorelocation value="{t}Browse{/t}">
	<BR>
	{if $sub <> ""}
	    <H3>{t 1=$sub}Restore of %1{/t}</H3>
	{/if}
	{foreach item=dir name=dir from=$loc}
	    {if $dir.type == "parent"}
	        <IMG SRC="/icons/back.gif" alt="[DIR]">
	    {elseif $dir.type == "dir"}
	        <IMG SRC="/icons/folder.gif" alt="[DIR]">
	    {elseif $dir.type == "deldir"}
	        <IMG SRC="/icons/folder.gif" alt="[DIR]">
	    {else}
	        <IMG SRC="/icons/generic.gif" alt="[   ]">
	    {/if}    
	    {if $dir.type == "file"}
		<A HREF="index.php?restorefile&client={$client}&sub={$dir.sub}&location={$location}">{$dir.name}</A>
	    {elseif $dir.type == "delfile"}
		<A HREF="index.php?restorefile&client={$client}&sub={$dir.sub}&location={$location}">{$dir.name}(deleted)</A>
	    {elseif $dir.type == deldir}
		<A HREF="index.php?restorelocation&client={$client}&sub={$dir.sub}&location={$location}">{$dir.name} ({t}Deleted{/t})</A>
	    {else}
		<A HREF="index.php?restorelocation&client={$client}&sub={$dir.sub}&location={$location}">{$dir.name}</A>
	    {/if}    
	    <BR>
	{/foreach}
	{foreach item=version name=version from=$revisions}
	    {if $version.type == "parent"}
	        <IMG SRC="/icons/back.gif" alt="[DIR]">
	    {else}
	        <IMG SRC="/icons/transfer.gif" alt="[   ]">
	    {/if}    
	    {if $version.type == "parent"}
		<A HREF="index.php?restorelocation&client={$client}&sub={$version.sub}&location={$location}">{$version.name}</A>
	    {elseif $version.type == "currentfile"}
		<A HREF="index.php?restorefile&client={$client}&file={$version.sub}&location={$location}&revision=current">current</A>
	    {elseif $version.type == "diff.gz"}
		<A HREF="index.php?restorefile&client={$client}&parent={$version.parent}&location={$location}&revision={$version.revision}&name={$version.name}">{$version.ts}</A>
	    {elseif $version.type == "snapshot.gz"}
		<A HREF="index.php?restorefile&client={$client}&parent={$version.parent}&location={$location}&revision={$version.revision}&name={$version.name}">{$version.ts}</A>
	    {else}
		unknown snapshot type $version.type
	    {/if}    
	    <BR>
	{/foreach}
    </FORM>
</HTML>
