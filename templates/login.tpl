{*
    slbackup-php, an administration tool for slbackup
    Copyright (C) 2007  Finn-Arne Johansen <faj@bzz.no>, Bzzware AS, Norway

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*}
    {include file="header.tpl"}

<div class="form">
    <FORM action="index.php" name=login method=POST>
	{t 1=$backupuser 2=$backuphost}To read the configuration, you must log in as %1 on %2.{/t}<BR>
	{t 1=$backupuser 2=$backuphost}Please enter the password for the %1 account on %2:{/t}<BR>
	{if $nonhttps == "warn"}
	    <B>
	    {t}WARNING: you are about to send you password over an unencrypted link, please ask your administrator to enable https{/t}
	    <BR>
	    </B>
        {/if}
	{if $nonhttps == "deny"}
	    <B>
	    {t}Login to this application is denied without the use of https. Please ask your administrator to enable https{/t}
	    <BR>
	    </B>
        {else}
	    <INPUT type=password name=Passwd><BR>
	    <INPUT type=submit name=login value="{t}Login{/t}">
	    <INPUT type=reset name=Reset value="{t}Reset{/t}">
        {/if}
    </FORM>
</div>
</html>
